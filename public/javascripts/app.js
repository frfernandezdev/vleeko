(function($) {
	// Accordion Menu
	let 
		root     =  'ul[data-accordion=""]',
		li       =  $(`${root} li.root > a`),
		sub_menu =  $(`${root} li ul.sub-menu`);

		sub_menu.hide()

		li.on('click', function(e){
			e.preventDefault()
			let url = $(this).next('ul.sub-menu');
			url.toggle('slow');
		})
	// Dropdown Custom
		let dropdown    = '[data-custom-dropdown]',
				subdropdown = '[data-subdropdown]',
				flag;
		
		$(dropdown).on('click', function(e){
			if($(this).data('custom-dropdown') === 'max-width')
				$(this).next().css({
					'min-width': $(this).outerWidth()
				})
			$(this)
				.attr('aria-expanded', $(this).attr('aria-expanded') === 'false' ? true: false)
				.parent().toggleClass('show')
		})
}(jQuery))